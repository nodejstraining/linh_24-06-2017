//tham khảo bài làm của Thức KATE
#ifndef HEADER_H
#define HEADER_H

#include <QObject>


class Testmylb : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString note READ note WRITE setmyobject NOTIFY noteChanged)

public:
    explicit Testmylb(QObject* parent = 0);
    QString note()
    {
        return msg;
    }

    void setmyobject(QString string);

signals:
    void noteChanged();

public slots:
    void Click();

private:
    int count;
    QString msg;
};


#endif // HEADER_H
