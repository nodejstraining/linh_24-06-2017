import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import testlib 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Testmylb{
        id: head;
    }

    Text{
        width: 100
        height: 100
        x: 300
        id: lalala
        text: head.note
    }

    Button{
        id: hellovietnam
        text: qsTr("Click me!")
        onClicked:
        {
            head.Click()
        }
    }

}
