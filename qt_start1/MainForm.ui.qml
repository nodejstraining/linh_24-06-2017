import QtQuick 2.5

Rectangle {
    property alias mouseArea: mouseArea

    width: 360
    height: 360

    MouseArea {
        id: mouseArea
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent

        TextInput {
            id: textInput1
            x: 140
            y: 120
            width: 80
            height: 22
            text: qsTr("haha")
            font.pixelSize: 12
        }

        Image {
            id: image1
            x: 130
            y: 217
            width: 100
            height: 100
            source: "../../Pictures/11-10-2014 04_54_25.jpg"
        }
    }

    Text {
        anchors.centerIn: parent
        text: "Hello World"
    }
}
