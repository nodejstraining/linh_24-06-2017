//tham khảo bài làm của Thức KATE
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QObject>
#include <QQmlContext>
#include "header.h"
#include <QtQml>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<Testmylb>("testlib", 1, 0, "Testmylb");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
