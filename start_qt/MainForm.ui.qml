import QtQuick 2.5

Rectangle {
    id: rectangle1
    property alias mouseArea: mouseArea

    width: 360
    height: 360

    MouseArea {
        id: mouseArea
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.left: parent.left
    }

    Text {
        anchors.centerIn: parent
        text: "Hello World"
    }
}
