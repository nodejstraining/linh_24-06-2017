#ifndef HEADERS_H
#define HEADERS_H

#include <QObject>

/*class DataStore : public QObject{
    Q_OBJECT
public:
    DataStore();


public slots:
    void Callme();
};
   */

class DataStore : public QObject{
    Q_OBJECT

public:
    explicit DataStore(QObject *parent = 0);

signals:
    void callex(QString ms);

public slots:
    void Callme();

private:
    int count;
    QString msg;
};

#endif // DATASTORE_H
